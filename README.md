It is an example to create a reproducible Python development environment with tries to remove struggles about deploying on a container orchestration system like `k8s`.

In this example, we prefer using the same container environment for both development and production to remove possible environmental issues. For simplicity's sake, a container environment itself would be enough without VM for this example but we are using Vagrant to run the container runtime on any OS.

**Running up**
```shell
$ vagrant up
$ # please visit http://localhost:3000 to see it in action.
```

**Watching logs**
```shell
$ vagrant ssh --command 'docker logs --follow app'
```

**Building a release**
```shell
$ vagrant ssh --command 'docker build /vagrant/src --build-arg=BUILD_ENV=release --tag app:v0.0.1 --tag app:latest'
```

**Deploying a release**
```shell
$ vagrant ssh --command 'k8s-deploy'
```

Note that you may need to install a vagrant plugin to automate guest additional installation if you are using `Virtualbox` for virtualization.
```shell
$ vagrant plugin install vagrant-vbguest
```

