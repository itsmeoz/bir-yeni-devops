#!/bin/bash

set -e

# start minikube
minikube start --driver=docker --ports=8080

# set docker env 
eval $(minikube docker-env)

# build release image for the app
docker build /vagrant/src --build-arg=BUILD_ENV=release --tag app:latest

# deploy the app on minikube
minikube kubectl -- apply -f /vagrant/deployment.yml

# use latest version of the image
minikube kubectl -- rollout restart deployment/my-app

# wait pods to get ready
sleep 10

echo "Please visit http://localhost:8080 to see it in action."

# we need to redirect app port due to we are using minikube for testing purposes
minikube kubectl -- port-forward --address=0.0.0.0 service/my-app 8080



