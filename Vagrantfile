# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/focal64"
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "4096"
    vb.cpus = 2
  end

  config.vm.network "forwarded_port", guest: 3000, host: 3000
  config.vm.network "forwarded_port", guest: 8080, host: 8080

  config.vm.provision "shell", inline: <<-SHELL
    # update package index
    apt-get update

    # install deps for the docker installation
    apt-get install -y \
      apt-transport-https \
      ca-certificates \
      curl \
      gnupg \
      lsb-release

    # download docker's package signing key
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

    # add docker's package repo
    echo \
      "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list

    # re-update package index
    apt-get update

    # install container runtime and docker
    apt-get install -y \
      docker-ce \
      docker-ce-cli \
      containerd.io

    # add vagrant user to docker group
    usermod -aG docker vagrant

    # start and enable docker daemon service
    systemctl enable --now docker

    # create mysql instance
    docker create \
      --restart=unless-stopped \
      --network=host \
      --env MYSQL_ROOT_PASSWORD=a-non-secure-password \
      --env MYSQL_DATABASE=app \
      --name db \
      mysql

    # start mysql instance
    docker start db

    # build development image for the app
    docker build /vagrant/src -t app:development

    # create development instance for the app
    docker create \
      --restart=unless-stopped \
      --network=host \
      --volume /vagrant/src:/app \
      --env MYSQL_USERNAME=root \
      --env MYSQL_PASSWORD=a-non-secure-password \
      --env MYSQL_INSTANCE_NAME=app \
      --env MYSQL_PORT_3306_TCP_ADDR=127.0.0.1 \
      --env MYSQL_PORT_3306_TCP_PORT=3306 \
      --name app \
      app:development

    # start development instance for the app
    docker start app

    # install minikube
    curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    install minikube-linux-amd64 /usr/local/bin/minikube

    # install the tool of k8s deployment demonstration
    ln -s /vagrant/k8s.sh /usr/local/bin/k8s-deploy
    chmod +x /usr/local/bin/k8s-deploy
  SHELL
end
